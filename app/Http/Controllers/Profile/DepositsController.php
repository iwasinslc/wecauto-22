<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAddFst;
use App\Http\Requests\RequestCreateDeposit;
use App\Models\AutoCreateDeposit;
use App\Models\Deposit;
use App\Models\Rate;
use App\Rules\DeposiLicenceLimit;
use App\Rules\RuleCheckRate;
use App\Rules\RuleDepositWalletExist;
use App\Rules\RuleEnoughBalance;
use App\Rules\RulePlanRange;
use App\Rules\RuleRateCurrency;
use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletExist;
use App\Rules\RuleWeekDepositLimit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class DepositsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $deposits = user()->deposits()->where('active', true)->get();
        return view('profile.deposits.index', [
            'deposits' => $deposits
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = (array)session()->get('data');

        return view('profile.deposits.create', [
            'data' => $data
        ]);
    }


    /**
     * @param RequestCreateDeposit $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function create_handle(Request $request)
    {
        $data = $request->all();

        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
            'type' => ['integer', 'required', 'min:0', 'max:3', new RuleWeekDepositLimit],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
//
//
//        cache()->put('deposit_' . user()->id, $data, now()->addMinutes(60));


        session()->put('data', $data);

        return redirect()->route('profile.deposits.add_photo');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add_photo()
    {
        $data = (array)session()->get('data');


        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
            'type' => ['integer', 'required', 'min:0', 'max:3', new RuleWeekDepositLimit],
        ]);
        if ($validator->fails()) {
            return redirect()->route('profile.deposits')->withErrors($validator->errors());
        }

        return view('profile.deposits.add_photo', [
            'data' => $data
        ]);
    }


    public function add_photo_handle(Request $request)
    {

        $validator = $request->validate([
            'image' => 'required|image|mimes:jpeg,bmp,png|max:5000',
        ]);

        $data = (array)session()->get('data');
        $path = $request->file('image')->storeAs(
            'photo', Str::random() . '.' . $request->file('image')->extension(), ['disk' => env('FILESYSTEM_DRIVER', 's3'), 'visibility' => 'public']
        );

        $data['image'] = $path;

        session()->put('data', $data);


        return redirect()->route('profile.deposits.add_amount');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add_amount()
    {
        $data = (array)session()->get('data');

        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
//            'image' => ['string', 'required', 'min:3'],
            'type' => ['integer', 'required', 'min:0', 'max:3', new RuleWeekDepositLimit],
        ]);
        if ($validator->fails()) {
            return redirect()->route('profile.deposits')->withErrors($validator->errors());
        }
        return view('profile.deposits.add_amount', [
            'data' => $data
        ]);
    }


    public function add_amount_handle(Request $request)
    {

        $data = (array)session()->get('data');


        $data['invested'] = $request->amount;
        $data['amount'] = $request->amount * 0.35 * \rate('USD', 'WEC');
        $data['wallet_id'] = $request->wallet_id;

        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
//            'image' => ['string', 'required', 'min:3'],
            'invested'=>['required', 'numeric' ,new DeposiLicenceLimit],
            'wallet_id' => ['required', new RuleDepositWalletExist, new RuleUUIDEqual],
            'amount' => ['numeric', new RuleEnoughBalance, 'min:0.0000001'],
            'type' => ['integer', 'required', 'min:0', 'max:3', new RuleWeekDepositLimit],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        session()->put('data', $data);


        return redirect()->route('profile.deposits.finish');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function finish()
    {




        $data = (array)session()->get('data');


        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
//            'image' => ['string', 'required', 'min:3'],
            'wallet_id' => ['required', new RuleDepositWalletExist, new RuleUUIDEqual],
            'amount' => ['numeric', new RuleEnoughBalance, 'min:0.0000001'],
            'type' => ['integer', 'required', 'min:0', 'max:3', new RuleWeekDepositLimit],
        ]);

        if ($validator->fails()) {
            return redirect()->route('profile.deposits')->withErrors($validator->errors());
        }


        return view('profile.deposits.finish', [
            'data' => $data,

        ]);
    }


    public function finish_handle(Request $request)
    {

//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }
        $data = (array)session()->get('data');

        $rate = Rate::query()->first();

        $data['rate_id'] = $rate->id;

        $data['agreement'] = $request->agreement;


        $validator = \Validator::make($data, [
            'name' => ['string', 'required', 'min:3'],
//            'image' => ['string', 'required', 'min:3'],
            'wallet_id' => ['required', new RuleDepositWalletExist, new RuleUUIDEqual],
            'amount' => ['numeric', new RuleEnoughBalance, 'min:0.0000001'],
            'agreement' => ['required'],
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }


        Deposit::addDeposit($data);


//        session()->foget(['data']);

        return redirect()->route('profile.deposits')->with('success', __('Deposit has been created'));
    }


    public function add_fst($id)
    {

        $active = \user()->activeLicence();

        if (!$active)
        {
            return redirect()->route('profile.profile')->with('error', __('You do not have an active license.'));

        }
        $deposit = Deposit::findOrFail($id);

        if($deposit->allowedExecutionDays() == 0) {
            return redirect()->route('profile.deposits')->with('error', __('Deposit is speed up maximum'));
        }

        return view('profile.deposits.add_fst', [
            'deposit' => $deposit
        ]);
    }

    /**
     * @param RequestCreateDeposit $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function add_fst_handle(RequestAddFst $request)
    {
        $active = \user()->activeLicence();

        if (!$active)
        {
            return redirect()->route('profile.profile')->with('error', __('You do not have an active license.'));
        }

//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }
        /**
         * @var Deposit $deposit
         */
        $deposit = Deposit::findOrFail($request->deposit_id);

        $deposit->addFst($deposit->maxNumberDays() - $request->count);

        return redirect()->route('profile.deposits')->with('success', __('Fst added succesfull'));
    }


    public function change_days($id, Request $request)
    {
        $deposit = Deposit::findOrFail($id);

        $data = [
            'percent'=>$deposit->progressPercentage($request->count),
            'end_date_month'=>$deposit->datetime_closing->subDays($request->count)->format('d/m'),
            'end_date_year'=>$deposit->datetime_closing->subDays($request->count)->format('y'),
        ];



        return response()->json($data);
    }


}
