<?php
namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/perfectmoney/status',
        '/payeer/status',
        '/webcoinapi/status',
        '/profile/topup/payment_message',
        '/etherapi/status',
        '/etherapiusdt/status',
        '/cryptocurencyapi/status',
        '/bip/status',
        '/prizm/status',
        '/custom/status',
        '/tronapiusdt/status',

        '/telegram_webhook/*',
        '/youtube/watch/save/*/*',
        '/logout',
    ];
}
