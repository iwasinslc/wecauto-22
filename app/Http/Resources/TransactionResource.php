<?php

namespace App\Http\Resources;

use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="TransactionResource",
 *     description="Transaction resource",
 *     type="object",
 *     @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Transaction"))
 * )
 */
class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Transaction $this */
        return [
            'id' => $this->id,
            'type' => $this->type->name,
            'value' => $this->amount,
            'currency' => $this->currency->code,
            'batch_id' => $this->batch_id,
            'order_id' => $this->order_id,
            'approved' => boolval($this->approved),
            'source' => $this->source,
            'result' => $this->result,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
