<?php
namespace App\Console\Commands\Automatic;

use App\Models\MailSent;
use App\Models\PageViews;
use App\Models\User;
use Illuminate\Console\Command;

/**
 * Class CleanPageViewsCommand
 * @package App\Console\Commands\Automatic
 */
class CleanPageViewsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:page_views';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean old page views';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $deleteFrom = now()->subHours(1)->toDateTimeString();

        PageViews::query()->where('created_at', '<', $deleteFrom)->delete();
    }
}
