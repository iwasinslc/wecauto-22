{{__('Currency')}} : {{$deposit->currency->code}}
{{__('Rate')}} : {{$deposit->rate->name}}
{{__('Earnings')}} : {{$deposit->daily}} % {{ __('per day') }}
{{__('Invested')}} : {{$deposit->invested}} {{$deposit->currency->symbol}}
{{__('Status')}} : {{$deposit->active ? __('active') : __('closed') }}
{{__('Closing')}} : {{\Carbon\Carbon::parse($deposit->created_at)->addDays($deposit->duration)->toDateString()}}