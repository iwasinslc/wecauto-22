@extends('layouts.profile')
@section('title', __('Account'))
@section('content')


    <section class="cars-and-partners">
        <div class="container">
            <div class="cars-and-partners__row">
                <div class="car-orders">
                    <h3 class="lk-title">{!! __('Vehicle orders') !!}
                    </h3>
                    <div class="car-orders__row">
                        @foreach($deposits as $deposit)
                        <div class="car-orders__col" data-type="car">
                            <div class="order-card order-card--active">
                                <div class="order-card-top">
                                    <div class="order-card-top__col">
                                        <p class="order-card-top__price">{{number_format($deposit->balance, 4)}} WEC
                                        </p>
{{--                                        <p class="order-card-top__price color-warning"> <i>342 FST</i>--}}
{{--                                        </p>--}}
                                    </div>
                                    <div class="order-card-top__col">
                                        <div class="clock-action">
                                            <div class="clock-action__top">
                                                <svg class="svg-icon">
                                                    <use href="/assets/icons/sprite.svg#icon-wall-clock"></use>
                                                </svg>
                                                <p>{{now()->diffInDays(\Carbon\Carbon::parse($deposit->datetime_closing))}} left</p>
                                            </div>
                                            <p class="clock-action__with">without <i class="color-warning">FST</i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-card__image"><img style="height: 100%" src="{{getPhotoPath($deposit->image)}}" alt="">
                                </div>
                                <div class="order-card__buttons">
                                    <!---->
                                    @if ($deposit->fst_in_usd>0)
                                        <p class="order-card__fst"><img src="/assets/images/fst-white2.png" alt=""><span>${{number_format($deposit->fst_in_usd, 2)}}</span>
                                        </p>
                                    @endif
                                    @if ($deposit->allowedExecutionDays())
                                    <!----><a class="apply-fst" href="{{route('profile.deposits.add_fst', ['id'=>$deposit->id])}}"><i>{{__('Apply')}} </i><img src="/assets/images/fst2.png" alt=""></a>
                                    @endif
                                </div>
                                <p class="order-card__title"><strong>{{$deposit->name}}</strong>
                                </p>
                            </div>
                        </div>
                        @endforeach
                        <div class="car-orders__col" data-type="car">
                            <div class="order-card">
                                <div class="order-card__top">
                                </div>
                                <div class="order-card__image"><img src="/assets/images/orders/car.png" alt="">
                                </div><a class="order-card__add" href="{{route('profile.deposits.create')}}"><span>{!! __('Add another vehicle') !!}</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="partners">
                    <h3 class="lk-title">{!! __('Partners') !!}
                    </h3>
                    <div class="partners__block">
                        <div class="typography">
                            <p>{{__('Last Partners')}}</p>
                        </div>
                        <table class="partners__table">
                            <tbody>
                            @foreach ($partners as $partner)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($partner->created_at)->format('d/m/Y')}}</td>
                                    <td>{{$partner->login}}</td>
                                    <td> {{$partner->pivot->line}} {{__('level')}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="partners__bottom"><a href="{{route('profile.affiliate')}}">{{__('Show all list')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="licenses">
        <div class="container">
            <h3 class="lk-title">{!! __('Licenses') !!}
            </h3>
            <div class="js-swiper-licenses swiper-container swiper-no-swiping">
                <div class="swiper-wrapper">
                    @foreach($licences as $licence)
                    <div class="swiper-slide">
                        <div class="license-item {{!user()->canBuyLicence()&&$licence->id==user()->licence_id? 'license-item--active' : ''}}">
                            <div class="license-item__icon">
                                <img src="/assets/images/licenses-icons/car{{$licence->id}}.svg" alt="">
                            </div>
                            <p class="license-item__name">{{__($licence->name)}} {{$licence->id}}
                            </p>
                            <div class="license-item__price">
                                <div class="license-item__price-count"><span><span>{{$licence->price}}</span><span class="color-warning">$</span></span>
                                </div>
                            </div>

                            <p class="license-item__subtitle">{!! __('Duration') !!} — <span class="color-warning">{{$licence->duration}} {!! __('days') !!}</span>
                            </p>

                            <ul class="license-item__list">
                                <li>{!! __('Price') !!} -  <strong> {{$licence->price*rate('USD', $licence->currency->code)}} {{$licence->currency->code}}  </strong>
                                </li>
{{--                                <li><strong> {{$licence->price*rate('USD', 'PZM')}} {{'PZM'}}  </strong>--}}
{{--                                </li>--}}
                                <li>{!! __('Minimum deposit price') !!} - <strong>${{$licence->deposit_min}}</strong></li>
                                <li>{!! __('Maximum deposit price') !!} - <strong>${{$licence->deposit_max}}</strong></li>
                                <li>{!! __('Maximum purchase amount FST') !!} <strong>${{$licence->buy_amount}}</strong>
                                </li>
                                <li>{!! __('Maximum FST Sale Amount') !!} <strong>${{$licence->sell_amount}}</strong>
                                </li>
                            </ul>
{{--                            <div class="license-item__desc">--}}
{{--                                <p>Car price $7000</p>--}}
{{--                                <p><small>(max)</small></p>--}}
{{--                            </div>--}}
                            <div class="license-item__buttons">
                                <button type="button" class="btn js-modal" data-modal="licences{{$licence->id}}">{{__('Activate')}}
                                </button>

                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="dots-pagination">   </div>
            </div>
        </div>
    </section>



@endsection
@section('modal_content')
    @foreach ($licences as $licence)
        <div class="modal" id="licences{{$licence->id}}">
            <div class="modal-inner modal-inner--dark">
                <div class="modal-inner__close js-close-modal">
                    <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-cross"></use>
                    </svg>
                </div>
                <div class="modal-header modal-header--border-bottom">
                    <h3 class="modal-title"><span>{{__('Are you sure?')}} </span>
                    </h3>
                </div>
                <form action="{{route('profile.buy_licence')}}" method="POST">
                    {{csrf_field()}}

                <div class="modal-content">

                    <div class="">
                        @if (user()->activeLicence()&&user()->licence!==null)
                            <p style="color:white">{{__('Your current license')}} "Licence {{user()->licence->id}}". {{__('When buying a license')}} "Licence {{user()->licence->id}}" {{__('a valid license will become invalid')}}</p>
                        @endif

                        <p>Licence {{$licence->id}} {!! __('(price - $'.number_format($licence->price).', validity-'.$licence->duration.' days).') !!}</p>
                        <p>{{ __('Maximum purchase amount FST')}} -  {{number_format($licence->buy_amount)}}$ {{ __('Maximum FST Sale Amount')}} - {{number_format($licence->sell_amount)}}$. </p>
                    </div>
{{--                    <input type="hidden" value="{{getUserWallet($licence->currency->code)->id}}"  name="wallet_id">--}}
                    <div class="balance-form__field">
                        <label>{{__('Currency')}}:</label>
                        <label class="select">
                            <select id="wallet_id" name="wallet_id">
                                <option value="{{getUserWallet($licence->currency->code)->id}}">{{$licence->currency->code}} {{number_format($licence->price*rate('USD', $licence->currency->code), 6)}}</option>
{{--                                <option value="{{getUserWallet('PZM')->id}}">PZM {{number_format($licence->price*rate('USD', 'PZM'), 2)}}</option>--}}
                            </select>
                        </label>
                    </div>
                </div>
                <div class="modal-footer modal-footer--center">


                    <button class="btn btn--green btn--size-lg"><span>{{__('Yes')}}</span></button>
                    <input type="hidden" name="licence_id" value="{{$licence->id}}">
{{--                        <input type="hidden" name="wallet_id" value="{{getUserWallet('WEC')->id}}">--}}

                    <button type="button" class="btn btn--warning btn--size-lg js-close-modal"><span>{{__('No')}}</span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection