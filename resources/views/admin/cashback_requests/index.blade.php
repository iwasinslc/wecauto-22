@extends('admin/layouts.app')
@section('title'){{ __('Cashback requests') }}@endsection
@section('breadcrumbs')
    <li> {{ __('Cashback requests') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Cashback requests') }}</h1>
                    <ul class="controls">
                        <li><a role="button" href="{{route('admin.cashback.export')}}">
                                <i class="fa fa-download"></i> {{ __('Export') }}
                            </a></li>
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table id="cashback-requests" class="js-datatables responsive nowrap no-sorting" data-page-length="25">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>{{ __('Created at') }}</th>
                                <th>{{ __('Login') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Telegram') }}</th>
                                <th>{{ __('Video link') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <style>
                                td.tdinput input {
                                    width: 100%;
                                }
                            </style>
                            <tr>
                                <th></th>
                                <th></th>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td class="tdinput"></td>
                                <td></td>
                                <td class="tdinput"></td>
                                <td></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->


        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection

@push('load-scripts')
    <script>
        //initialize basic datatable
        var table = $('#cashback-requests').DataTable({
            "iDisplayLength": 25,
            "processing": true,
            "serverSide": true,
            "order": [[1, "desc"]],
            "ajax": '{{ route('admin.cashback.dataTable') }}',
            "columns": [
                {
                    "data": "DT_Row_Index", "orderable": false,"searchable": false , "render": function (data, type, row, meta) {
                        return meta.settings._iRecordsTotal-data+1;
                    }
                },
                {"data": "created_at"},
                {
                    "data": "user.login", "render": function (data, type, row) {
                        return '<a href="' + row['user_id'] + '" target="_blank">' + data + '</a>';
                    }
                },
                {
                    "data": "user.email", "render": function (data) {
                        return '<a href="mailto:' + data + '" target="_blank">' + data + '</a>';
                    }
                },
                {"data": "telegram"},
                {
                    "data": "video_link",
                    "render": function (data) {
                        return '<a href="' + data + '" target="_blank">' + data + '</a>';
                    }
                },
                {"data": "status"},
                {
                    "data": 'action',
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return '<a href="' + row['show'] + '" target="_blank" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a>'
                            + '<a href="' + row['profile'] + '" target="_blank" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i> {{ __('profile') }}</a>';
                    }
                }
            ],
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': ["no-sort"],
                    'render': function (data, type, row) {
                        return data + ' (' + row[1] + ')';
                    },
                }],
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': ["no-sort"],}
            ],
            "dom": 'Rlfrtip',
            initComplete: function () {
                this.api().columns([2, 3, 4]).every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty()).attr('placeholder', '{{ __('search ...') }}')
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });

        //*initialize basic datatable
    </script>
@endpush