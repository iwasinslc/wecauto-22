<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingSeeder
 */
class LicencesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $prices = [
            1 => [
                'name'=>'Licence',
                'duration'=>120,
                'price'=>50,
                'daily_limit'=>1,
                'deposit_max'=>8000,
                'currency_id'=>\App\Models\Currency::getByCode('WEC')->id,
                'deposit_min'=>5000,
                'moto_min'=>1000,
                'buy_amount'=>500,
                'sell_amount'=>250,
            ],
            2 => [
                'name'=>'Licence',
                'duration'=>180,
                'price'=>500,
                'daily_limit'=>2,

                'deposit_max'=>25000,
                'currency_id'=>\App\Models\Currency::getByCode('WEC')->id,
                'deposit_min'=>5000,
                'moto_min'=>1000,
                'buy_amount'=>5000,
                'sell_amount'=>2500,
            ],
            3 => [
                'name'=>'Licence',
                'duration'=>250,
                'price'=>5000,
                'daily_limit'=>3,
                'deposit_max'=>80000,
                'currency_id'=>\App\Models\Currency::getByCode('WEC')->id,
                'deposit_min'=>5000,
                'moto_min'=>1000,
                'buy_amount'=>50000,
                'sell_amount'=>25000,
            ],
            4 => [
                'name'=>'Licence',
                'duration'=>300,
                'price'=>20000,
                'daily_limit'=>4,
                'deposit_max'=>250000,
                'currency_id'=>\App\Models\Currency::getByCode('ACC')->id,
                'deposit_min'=>5000,
                'moto_min'=>1000,
                'buy_amount'=>200000,
                'sell_amount'=>100000,
            ],
            5     => [
                'name'=>'Licence',
                'duration'=>360,
                'price'=>50000,
                'daily_limit'=>5,
                'deposit_max'=>500000,
                'currency_id'=>\App\Models\Currency::getByCode('ACC')->id,
                'deposit_min'=>5000,
                'moto_min'=>1000,
                'buy_amount'=>500000,
                'sell_amount'=>250000,
            ],


        ];



        foreach ($prices as $key => $arr) {
            $checkExists = DB::table('licences')->where('id', $key)->count();

            if ($checkExists > 0) {
                echo "Price '".$key."' already registered.\n";
                continue;
            }

            DB::table('licences')->insert([
                'id'=>$key,
                'name'=>$arr['name'],
                'duration'=>$arr['duration'],
                'price'=>$arr['price'],
                'daily_limit'=>$arr['daily_limit'],
                'deposit_max'=>$arr['deposit_max'],
                'currency_id'=>$arr['currency_id'],
                'deposit_min'=>$arr['deposit_min'],
                'moto_min'=>$arr['moto_min'],
                'buy_amount'=>$arr['buy_amount'],
                'sell_amount'=>$arr['sell_amount'],
            ]);
            echo "Price '".$key."' registered.\n";
        }
    }
}
