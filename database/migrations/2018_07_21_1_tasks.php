<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function(Blueprint $table) {
            $table->string('id')->primary();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->float('reward_amount')->default(0);
            $table->string('reward_payment_system_id')->nullable();
            $table->string('reward_currency_id')->nullable();
            $table->integer('duration')->default(0);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
